import '../styles/Chat.css'
import { InputMessages } from './InputMessage'
import { Message } from '../components/Message'
import userImage from '../assets/photo.svg'
import { useEffect, useState } from 'react'

export function Chat() {
  const [userName, setUserName] = useState(
    JSON.parse(localStorage.getItem('userName')).userName || ''
  )

  function createUser(userName) {
    const data = { userName: userName }
    const options = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    }

    fetch('http://localhost:8080/user', options)
      .then(response => response.json())
      .then(data => {
        setUserName(data.userName)
        localStorage.setItem('userName', JSON.stringify(data))
      })
  }

  function getUser(userName) {
    fetch(`http://localhost:8080/getUser/${userName}`)
      .then(response => response.json())
      .then(data => {
        console.log(data)
      })
  }

  function Login() {
    if (getUser(userName)) {
      console.log('User already logged in')
    } else {
      createUser(userName)
    }
  }

  return (
    <div className="Chat">
      <div className="header-container">
        <header>
          <button id="status-btn">
            <div id="user-img" className="online">
              <img src={userImage} alt="User Photo" />
            </div>
          </button>
          <form
            action=""
            className="form-login"
            onSubmit={event => event.preventDefault()}
          >
            <input
              type="text"
              className="input-login"
              value={userName}
              onChange={event => {
                setUserName(event.target.value)
              }}
            />
            <button onClick={Login}>dasdas</button>
          </form>
        </header>
      </div>
      <div className="messages-container">
        <Message />
        <InputMessages />
      </div>
    </div>
  )
}
