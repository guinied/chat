import { BiSend } from 'react-icons/bi'
import { useState } from 'react'
import '../styles/InputMessage.css'
export function InputMessages() {
  const [message, setMessage] = useState('')
  const userName = JSON.parse(localStorage.getItem('userName')).userName
  console.log(userName)

  function sendMessage() {
    const data = { message: message }

    const options = {
      method: 'POST',
      body: JSON.stringify(data),

      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    }

    fetch(`http://localhost:8080/message/${userName}`, options)
      .then(response => response.json())
      .then(data => {
        setMessage('')
      })
      .then(userData => {
        console.log(userData)
      })
  }

  return (
    <>
      <form action="" onSubmit={event => event.preventDefault()}>
        <div className="input-container">
          <input
            type="text"
            onChange={event => setMessage(event.target.value)}
            value={message}
            className="input-message"
          />
          <button
            className=""
            onClick={() => {
              sendMessage(message)
              setMessage('')
            }}
          >
            <BiSend />
          </button>
        </div>
      </form>
    </>
  )
}
