import { useState, useEffect } from 'react'
import '../styles/Messages.css'
export function Message() {
  const [messages, setMessages] = useState([])

  useEffect(() => {
    setInterval(() => {
      fetch('http://localhost:8080/listMessages')
        .then(response => {
          return response.json()
        })
        .then(data => {
          setMessages(data)
        })
    }, 500)
  }, [])

  return (
    <div className="Message">
      {messages?.map(message => (
        <div id="message-wrapper" key={message.id}>
          <div id="user-container"></div>

          <div id="message-container" key={message.id}>
            <p key={message.id}>{message.text}</p>
          </div>
        </div>
      ))}
    </div>
  )
}
