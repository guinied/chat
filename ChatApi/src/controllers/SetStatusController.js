const SetStatusService = require('../services/SetStatusService')

class SetStatusController {
  async handle(request, response) {
    const { userName, status } = request.body

    const service = new SetStatusService()

    const result = await service.execute(userName, status)

    return response.json(result)
  }
}

module.exports = SetStatusController
