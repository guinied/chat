const CreateUserService = require('../services/CreateUserService')

class CreateUserController {
  async handle(request, response) {
    const { userName } = request.body
    console.log(userName)
    const service = new CreateUserService()
    const result = await service.execute(userName)

    return response.json(result)
  }
}

module.exports = CreateUserController
