const GetUserService = require('../services/GetUserService')

class GetUserController {
  async handle(request, response) {
    const { userName } = request.params
    console.log(userName)

    const service = new GetUserService()

    const result = await service.execute(userName)

    return response.json(result)
  }
}

module.exports = GetUserController
