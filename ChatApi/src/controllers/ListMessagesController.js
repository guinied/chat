const ListMessagesService = require('../services/ListMessagesService')

class ListMessagesController {
  async handle(request, response) {
    const service = new ListMessagesService()

    const result = await service.execute()

    return response.json(result)
  }
}

module.exports = ListMessagesController
