const CreateMessageService = require('../services/CreateMessageService')

class CreateMessageController {
  async handle(request, response) {
    const { message, type } = request.body
    const { userName } = request.params

    const service = new CreateMessageService()

    const result = await service.execute(message, userName, type)

    return response.json(result)
  }
}

module.exports = CreateMessageController
