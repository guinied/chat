const express = require('express')

const CreateMessageController = require('./controllers/CreateMessageController')
const CreateUserController = require('./controllers/CreateUserController')
const ListMessagesController = require('./controllers/ListMessagesController')
const SetStatusController = require('./controllers/SetStatusController')
const GetUserController = require('./controllers/GetUserController')

const router = express.Router()

router.post('/message/:userName', new CreateMessageController().handle)
router.post('/user', new CreateUserController().handle)
router.post('/setStatus', new SetStatusController().handle)
router.get('/listMessages', new ListMessagesController().handle)
router.get('/getUser/:userName', new GetUserController().handle)

module.exports = router
