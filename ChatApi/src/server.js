const express = require('express')
const router = require('./routes')
const cors = require('cors')
const app = express()
app.use(cors('*'))
app.use(express.json())

app.use(router)

app.get('/', (req, res) => {
  res.send('Hello Word')
})

app.listen('8080', () => console.log('Server is running on 8080🚀'))
