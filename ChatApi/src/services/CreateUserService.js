const { PrismaClient } = require('@prisma/client')

const prismaClient = new PrismaClient()
class CreateUserService {
  async execute(userName) {
    const user = await prismaClient.user.create({
      data: {
        userName
      }
    })

    return user
  }
}
module.exports = CreateUserService
