const { PrismaClient } = require('@prisma/client')

const prismaClient = new PrismaClient()
class CreateMessageService {
  async execute(text, userName, type) {
    const message = await prismaClient.message.create({
      data: {
        text,
        user: {
          connect: {
            userName
          }
        },
        receiver: 'Fabinho'
      }
    })

    return message
  }
}
module.exports = CreateMessageService
