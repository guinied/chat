const { PrismaClient } = require('@prisma/client')
const { name } = require('ejs')

const prismaClient = new PrismaClient()
class SetStatusService {
  async execute(status, userName) {
    const message = await prismaClient.status.create({
      data: {
        user: {
          connect: {
            userName
          }
        },
        status
      }
    })

    return message
  }
}
module.exports = SetStatusService
