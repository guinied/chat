const { PrismaClient } = require('@prisma/client')

const prismaClient = new PrismaClient()
class GetUserService {
  async execute(userName) {
    const user = await prismaClient.user.findUnique({
      where: {
        userName
      }
    })

    return user
  }
}
module.exports = GetUserService
