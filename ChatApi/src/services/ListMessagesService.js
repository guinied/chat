const { PrismaClient } = require('@prisma/client')

const prismaClient = new PrismaClient()
class ListMessagesService {
  async execute(name, id, user) {
    const messages = await prismaClient.message.findMany({
      orderBy: {
        id,
        user: {
          name
        }
      },
      include: {
        user
      }
    })

    return messages
  }
}
module.exports = ListMessagesService
